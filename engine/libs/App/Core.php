<?php

namespace App {
    class Core {
        protected static $_instance;

        private function __construct(){}

        private function __clone(){}

        public static function getInstance() {
            if (null === self::$_instance) {
                self::$_instance = new self();
            }
            return self::$_instance;
        }

        public static function boot(){
            foreach ($_GET as $key => $value) {
                switch($key) {
                    case 'array':
                        $params = explode(',', $value);
                        if (count($params) == 3) {
                            $array = new RArray($params[0], $params[1], $params[2]);
                            $array->toJSON();
                        }
                        break;

                    case 'matrix':
                        $params = explode(',', $value);
                        if (count($params) == 2) {
                            $matrix = new Matrix($params[0], $params[1]);
                            $matrix->toJSON();
                        }
                        break;

                    case 'conquest':
                        $params = explode(',', $value);
                        if (count($params) == 3){
                            $matrix = new Matrix($params[0], $params[1]);
                            $pos = explode(':', str_replace(array('(', ')'), '', $params[2]));
                            --$pos[0];
                            --$pos[1];
                            $matrix->conquest($pos);
                            $matrix->toJSON();
                        }
                        break;
                }
            }
        }
    }

}