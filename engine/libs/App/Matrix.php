<?php
namespace App {

    /**
     * Class Matrix
     * @package App
     */
    class Matrix {

        /**
         * @var number of cols in matrix
         */
        private $cols;
        /**
         * @var number of rows in matrix
         */
        private $rows;
        /**
         * @var array is matrix
         */
        private $matrix;

        /**
         * @var int count of turns
         */
        private $count=0;

        /**
         * @var int shown steps of making matrix not nulled
         */
        private $steps;

        /**
         * Constructor of class
         * @param $cols number of rows in matrix
         * @param $rows number of cols in matrix
         */
        public function __construct($cols, $rows) {
            $this->cols = $cols;
            $this->rows = $rows;
            $this->matrix = $this->make();
            $this->steps = array();
        }

        /**
         * Making matrix
         * @param int $content what we put in array element
         * @return array maked array
         */
        private function make($content=0) {
            $array = array();
            for ($i = 0; $i < $this->rows; $i++) {
                for ($j = 0; $j < $this->cols; $j++) {
                    $array[$i][$j] = $content;
                }
            }
            return $array;
        }

        /**
         * Conquest by some content
         * @param array $pos
         * @param int $content
         */
        public function conquest($pos=array(), $content=1) {
            if (empty($pos)) {
                $pos[0] = mt_rand(0, $this->rows-1);
                $pos[1] = mt_rand(0, $this->cols-1);
            }
            if (is_array($pos)){
                if (count($pos) == 2){
                    $this->matrix[$pos[0]][$pos[1]] = $content;
                    $this->relatives($pos);
                }
            }
        }

        /**
         * Function returns matrix to string
         * @param array $matrix what matrix we need to convert to string
         */
        public function toString($matrix=array()) {
            if (empty($matrix)){
                $matrix = $this->matrix;
            }
            echo '<br>Matrix<br><br>';
            for ($i = 0; $i < $this->rows; $i++) {
                for ($j = 0; $j < $this->cols; $j++) {
                    echo $matrix[$i][$j].'&nbsp;';
                }
                echo '<br>';
            }
            if ($this->count > 0) {
                echo 'Count of relatives is '.$this->count;
            }
            echo '<br><br>';
        }

        /**
         * Function return matrix and data to JSON
         * @param array $matrix
         */
        public function toJSON($matrix=array()){
            if (empty($matrix)){
                $matrix = $this->matrix;
            }
            $result = [
                'matrix' => [$matrix],
                'count' => $this->count,
                'steps' => $this->steps
            ];

            echo json_encode($result);
        }

        /**
         * Found relatives and put to those elements some content
         * @param array $pos
         * @param int $content
         * @param int $iterator
         * @return bool
         */
        private function relatives($pos=array(), $content=1, $iterator=1){
            ++$this->count;

            if (empty($pos)) {
                $pos[0] = mt_rand(0, $this->rows - 1);
                $pos[1] = mt_rand(0, $this->cols - 1);
            }

            if (is_array($pos) and count($pos) == 2 and $pos[0] >= 0 and  $pos[0] < $this->rows
                and $pos[1] >= 0 and $pos[1] < $this->cols)
            {
                if (($this->rows - $iterator >= 0) or ($this->cols - $iterator >= 0)
                    or ($pos[0] + $iterator <= $this->cols) or ($pos[0] + $iterator <= $this->cols))
                {
                    $minI = ($pos[0] - $iterator >= 0) ? -$iterator : -$pos[0];
                    $minJ = ($pos[1] - $iterator >= 0) ? -$iterator : -$pos[1];

                    $maxI = ($pos[0] + $iterator <= $this->rows) ? $iterator : $this->rows-$pos[0]-1;
                    $maxJ = ($pos[1] + $iterator <= $this->cols) ? $iterator : $this->cols-$pos[1]-1;

                    for ($i = $minI; $i <= $maxI; $i++){
                        for ($j = $minJ; $j <= $maxJ; $j++) {
                            $_i = $pos[0]+$i;
                            $_j = $pos[1]+$j;
                            if (array_key_exists($_i, $this->matrix) and array_key_exists($_j, $this->matrix[$_i])
                            and $this->matrix[$_i][$_j] != $content){
                                $this->matrix[$_i][$_j] = $content;
                            }
                        }
                    }

                    array_push($this->steps, $this->matrix);

                    if (DEBUG_MODE) {
                        $this->toString($this->matrix);
                    }

                    if (((abs($minI)+$maxI+1) >= $this->rows) and (abs($minJ)+$maxJ+1) >= $this->cols) return false;
                    else return $this->relatives($pos, $content, $iterator+1);
                } else {
                    return false;
                }
            }
        }
    }

}