<?php

namespace App {
    /**
     * Class Core
     * using for creation random array (RArray) in
     * gns-it-tests web.app
     * @package App
     */
    class RArray
    {

        /**
         * @var int size of RArray
         */
        private $size;
        /**
         * @var int minimum of random function range
         */
        private $min;
        /**
         * @var int minimum of random function range
         */
        private $max;

        /**
         * @var - RArray;
         */
        private $array;

        /**
         * Create array
         * @param int $size - size of array
         * @param int $min - minimum of random function range
         * @param int $max - maximum of random function range
         */
        public function __construct($size = 1, $min = 1, $max = 999)
        {
            $this->size = $size;
            $this->min = $min;
            $this->max = $max;
            $this->put();
        }

        /**
         * Set random values in array
         * @param int $itterator
         */
        private function put($itterator = 0)
        {
            if ($this->size > 0) {
                $this->array[$itterator] = mt_rand($this->min, $this->max);
                if (++$itterator < $this->size) {
                    $this->put($itterator);
                }
            }
        }

        /**
         * Returns all info about array in HTML
         */
        public function toString()
        {

            echo 'Size of array - ' . $this->size . '<br>';
            echo 'Min value of element - ' . $this->min . '<br>';
            echo 'Max value of element - ' . $this->max . '<br>';

            echo '<pre>';
            print_r($this->array);
            echo '</pre>';
        }

        /**
         * Returns all info about array in JSON
         */
        public function toJSON()
        {
            $result = [
                'array' => [$this->array],
                'size' => $this->size,
                'values' => [
                    'min' => $this->min,
                    'max' => $this->max
                ]
            ];

            echo json_encode($result);
        }
    }
}