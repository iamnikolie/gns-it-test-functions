# README #

Обязательно прочитать

## Описание API ##

* engine/array

Параметры: количество_элементов,минимум_значения_элемента,максимум_значения_элемента ([пример](http://imn.co.ua/gns-it-test-functions/engine/array/213,3,10))

* engine/matrix

Параметры: количество_строчек,количество_столбиков
([пример](http://imn.co.ua/gns-it-test-functions/engine/matrix/5,5))

* engine/conquest

Параметры: количество_строчек,количество_столбиков,(индекс_начала_по_строчке:индекс_начала_по_столбику)
([пример](http://imn.co.ua/gns-it-test-functions/engine/conquest/5,5,(1:2)))