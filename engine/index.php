<?php

define('ROOTPATH', dirname(__FILE__));
define('DEBUG_MODE', false);
require_once ('libs/Autoloader.php');

use App\Core;
use App\RArray;
use App\Matrix;

Core::boot();